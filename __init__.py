from __future__ import absolute_import
from __future__ import print_function

# Avoid importing "expensive" modules here (e.g. scipy), since this code is
# executed on PyMOL's startup. Only import such modules inside functions.

import os
from pymol.cgo import *

def __init_plugin__(app=None):
    '''
    Pymol Box Creator
    '''
    from pymol.plugins import addmenuitemqt
    addmenuitemqt('Box Creator', run_plugin_gui)


# global reference to avoid garbage collection of our dialog
dialog = None


def run_plugin_gui():
    '''
    Box Creator Dialog
    '''
    global dialog

    if dialog is None:
        dialog = make_dialog()

    dialog.show()


def make_dialog():
    # entry point to PyMOL's API
    from pymol import cmd

    # pymol.Qt provides the PyQt5 interface, but may support PyQt4
    # and/or PySide as well
    from pymol.Qt import QtWidgets
    from pymol.Qt.utils import loadUi
    from pymol.Qt.utils import getSaveFileNameWithExt

    # create a new Window
    dialog = QtWidgets.QDialog()

    # populate the Window from our *.ui file which was created with the Qt Designer
    uifile = os.path.join(os.path.dirname(__file__), 'main.ui')
    form = loadUi(uifile, dialog)
    
    def draw():
        minX = form.Box_x.value() - form.Box_dx.value() / 2
        minY = form.Box_y.value() - form.Box_dy.value() / 2
        minZ = form.Box_z.value() - form.Box_dz.value() / 2
        maxX = form.Box_x.value() + form.Box_dx.value() / 2
        maxY = form.Box_y.value() + form.Box_dy.value() / 2
        maxZ = form.Box_z.value() + form.Box_dz.value() / 2



        boundingBox = [
            LINEWIDTH, float(2.0),

                BEGIN, LINES,
            COLOR, float(1), float(1), float(1),

                VERTEX, minX, minY, minZ,       #1
            VERTEX, minX, minY, maxZ,       #2

                VERTEX, minX, maxY, minZ,       #3
            VERTEX, minX, maxY, maxZ,       #4

                VERTEX, maxX, minY, minZ,       #5
            VERTEX, maxX, minY, maxZ,       #6

                VERTEX, maxX, maxY, minZ,       #7
            VERTEX, maxX, maxY, maxZ,       #8


                VERTEX, minX, minY, minZ,       #1
            VERTEX, maxX, minY, minZ,       #5

                VERTEX, minX, maxY, minZ,       #3
            VERTEX, maxX, maxY, minZ,       #7

                VERTEX, minX, maxY, maxZ,       #4
            VERTEX, maxX, maxY, maxZ,       #8

                VERTEX, minX, minY, maxZ,       #2
            VERTEX, maxX, minY, maxZ,       #6


                VERTEX, minX, minY, minZ,       #1
            VERTEX, minX, maxY, minZ,       #3

                VERTEX, maxX, minY, minZ,       #5
            VERTEX, maxX, maxY, minZ,       #7

                VERTEX, minX, minY, maxZ,       #2
            VERTEX, minX, maxY, maxZ,       #4

                VERTEX, maxX, minY, maxZ,       #6
            VERTEX, maxX, maxY, maxZ,       #8

                END
        ]
        cmd.load_cgo(boundingBox, "_temp_box")

    def update():
        ori = cmd.get("auto_zoom")
        cmd.set("auto_zoom", "0")
        cmd.delete("_temp_box")
        draw()
        cmd.set("auto_zoom", ori)

    def create():
        cmd.delete("_temp_box")
        
        minX = form.Box_x.value() - form.Box_dx.value() / 2
        minY = form.Box_y.value() - form.Box_dy.value() / 2
        minZ = form.Box_z.value() - form.Box_dz.value() / 2
        maxX = form.Box_x.value() + form.Box_dx.value() / 2
        maxY = form.Box_y.value() + form.Box_dy.value() / 2
        maxZ = form.Box_z.value() + form.Box_dz.value() / 2



        boundingBox = [
            LINEWIDTH, float(2.0),

                BEGIN, LINES,
            COLOR, float(1), float(1), float(1),

                VERTEX, minX, minY, minZ,       #1
            VERTEX, minX, minY, maxZ,       #2

                VERTEX, minX, maxY, minZ,       #3
            VERTEX, minX, maxY, maxZ,       #4

                VERTEX, maxX, minY, minZ,       #5
            VERTEX, maxX, minY, maxZ,       #6

                VERTEX, maxX, maxY, minZ,       #7
            VERTEX, maxX, maxY, maxZ,       #8


                VERTEX, minX, minY, minZ,       #1
            VERTEX, maxX, minY, minZ,       #5

                VERTEX, minX, maxY, minZ,       #3
            VERTEX, maxX, maxY, minZ,       #7

                VERTEX, minX, maxY, maxZ,       #4
            VERTEX, maxX, maxY, maxZ,       #8

                VERTEX, minX, minY, maxZ,       #2
            VERTEX, maxX, minY, maxZ,       #6


                VERTEX, minX, minY, minZ,       #1
            VERTEX, minX, maxY, minZ,       #3

                VERTEX, maxX, minY, minZ,       #5
            VERTEX, maxX, maxY, minZ,       #7

                VERTEX, minX, minY, maxZ,       #2
            VERTEX, minX, maxY, maxZ,       #4

                VERTEX, maxX, minY, maxZ,       #6
            VERTEX, maxX, maxY, maxZ,       #8

                END
        ]
        cmd.load_cgo(boundingBox, form.Box_name.text())
        print("size_x = " + str(round(form.Box_dx.value(), 4)))
        print("size_y = " + str(round(form.Box_dy.value(), 4)))
        print("size_z = " + str(round(form.Box_dz.value(), 4)))
        print("center_x = " + str(round(form.Box_x.value(), 4)))
        print("center_y = " + str(round(form.Box_y.value(), 4)))
        print("center_z = " + str(round(form.Box_z.value(), 4)))
        
        
    form.Button1.clicked.connect(draw)
    form.Box_x.valueChanged.connect(lambda: update())
    form.Box_y.valueChanged.connect(lambda: update())
    form.Box_z.valueChanged.connect(lambda: update())
    form.Box_dx.valueChanged.connect(lambda: update())
    form.Box_dy.valueChanged.connect(lambda: update())
    form.Box_dz.valueChanged.connect(lambda: update())
    form.Button1.clicked.connect(create)
    return dialog
